.. index::
   double: versions; pyheimdall
   single: pyheimdall
.. _pyheimdall:



pyHeimdall
==========

|doi-pyheimdall|

**pyHeimdall** is an implementation of :ref:`Heimdall <home>` for the `Python programming language <https://www.python.org/>`_.

.. seealso::

   There is another implementation of |project| for the XQuery programming language:: :ref:`xheimdall`.


.. _pyheimdall_install:

Install
-------

.. image:: https://img.shields.io/pypi/v/pyheimdall
   :target: https://pypi.org/project/pyheimdall/
   :alt: PyPI - Python Version

| This software is available as a `PyPI package <https://pypi.org/project/pyheimdall>`_ named ``pyheimdall``.
| You can install it using the `pip <https://pip.pypa.io/en/stable/>`_ package manager:

.. code-block:: bash

   pip install pyheimdall



.. _pyheimdall_quickstart:

Quickstart
----------

To use Heimdall's python modules, you must first import them.
The main module is name :py:mod:`heimdall`.

.. code-block:: python

   import heimdall

To retrieve the contents of a database in format ``format`` from location ``url``, just use the :py:func:`heimdall.getDatabase` function:

.. code-block:: python

   tree = heimdall.getDatabase(format=format, url=url)

To export a database contents ``tree`` to the format ``format`` at location ``url``, just use the function :py:func:`heimdall.createDatabase`:

.. code-block:: python

   heimdall.createDatabase(tree, format=format, url=url)

For example, the following program imports a XML database, then exports it as a SQL dump:

.. code-block:: python

   import heimdall

   MENAGERIE_XML = 'https://gitlab.huma-num.fr/datasphere/heimdall/python/-/raw/main/tests/examples/menagerie/hera.xml'
   tree = heimdall.getDatabase(format='hera:xml', url=MENAGERIE_XML)
   heimdall.createDatabase(tree, format='sql:mariadb', url='menagerie.sql')



Wanna learn more?
-----------------

To discover pyHeimdall features, you can read the `reference manual <modules.html>`_.
Main modules :py:mod:`heimdall` and :py:mod:`heimdall.util` are of particular interest.



.. toctree::
   :hidden:

   modules
   /modindex

.. include:: /doi.rst

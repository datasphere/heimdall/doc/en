.. index::
   double: versions; xheimdall
   single: xheimdall
.. _xheimdall:



xHeimdall
==========

|doi-xheimdall|

**xHeimdall** is an implementation of :ref:`Heimdall <home>` in the `XQuery programming language <https://en.wikipedia.org/wiki/XQuery>`_.

.. seealso::

   There is another implementation of |project| for the Python programming language:: :ref:`pyheimdall`.



.. _xheimdall_install:

Install
-------

.. note::

   There is only support for BaseX.
   If you have `another XQuery implementation <https://en.wikipedia.org/wiki/XQuery#Implementations>`_ in mind, feel free to :ref:`contribute <contributing>`.

If you have `BaseX installed <https://docs.basex.org/wiki/Startup>`_, inside the user interface, you can `install the package <https://docs.basex.org/wiki/Repository#Installation>`_ like this:

.. code-block::

   REPO INSTALL https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar

If BaseX complains "``[repo:not-found] Package ... heimdall.xar not found.``", you can temporarily set ``IGNORECERT=true`` in your `BaseX configuration file <https://docs.basex.org/wiki/Configuration>`_ and retry.

As an alternative, you can manually download the package and then install it using its local path.
Using the command like interface, it can work like this:

.. code-block:: bash

   curl -k https://datasphere.gitpages.huma-num.fr/heimdall/xquery/heimdall.xar --output heimdall.xar
   basex -c"<repo-install path='heimdall.xar'/>"



.. _xheimdall_quickstart:

Quickstart
----------

.. note::

   These code snippets apply to Heimdall version 3 and greater.

   For version 2, just replace:

   * ``heimdall:getDatabase`` by ``heimdall:getDatabaseItems``,
   * ``heimdall:createDatabase`` by ``heimdall.serialize``.

To retrieve the contents of a database in format ``format`` from location ``url``, just use the ``heimdall:getDatabase`` function:

.. code-block:: xquery

   heimdall:getDatabase(map {
     'url': url,
     'format': format
   })

To export a database contents ``$tree`` to the format ``format``, use the ``heimdall:createDatabase`` function:

.. code-block:: xquery

   heimdall:createDatabase($items, map {
     'format': format
   })

For example, the following codes imports a MySQL database, then exports it as CSV:

.. code-block:: xquery

   let $tree := heimdall:getDatabase(map {
     'url': 'mysql://localhost',
     'database': 'your_db_name_here',
     'format': 'sql:mysql'
     })
   return
   heimdall:createDatabase($tree, map {
     'format': 'csv'
     })



Wanna learn more?
-----------------

To discover xHeimdall features, you can read the `reference manual <https://datasphere.gitpages.huma-num.fr/heimdall/xquery/doc/heimdall.html>`_ (in french).


.. include:: /doi.rst

.. [#ignorecert]
   https://docs.basex.org/wiki/Options#IGNORECERT

.. _home:

#########
|project|
#########

|project| is a tool for converting more easily one or more databases from one format to another.

It makes a *technical* no-brainer migration and interoperability between SQL databases (`MariaDB <https://mariadb.org/>`_, `MySQL <https://www.mysql.com/fr/>`_), `REST APIs <https://en.wikipedia.org/wiki/REST>`_, and general-purpose exchange formats such as `CSV <https://en.wikipedia.org/wiki/Comma-separated_values>`_ (used for example by `spreadsheets <https://www.libreoffice.org/discover/calc/>`_ or `Omeka <https://omeka.org/s/>`_) or `XML <https://en.wikipedia.org/wiki/XML>`_.



Use cases
=========

If, for example, you find yourself in one of the following situations…

* you need access to data, but it is not stored in a format compatible with your favorite software…
* you'd prefer to merge several heterogeneous databases into a single corpus, easier to analyze…
* you're considering which input format to use for your software or database…
* you want to make your data more accessible and interoperable…

… then |project| can offer you an exchange format, and abstract the details of data implementation: yours, but also those of others.
Thanks to |project|, you can also switch from one technology to another in a matter of moments, as and when you need to, without ever losing, disorganizing or corrupting your data –or even by improving them over time, documenting them and linking them to current standards in your field.

In a nutshell, |project| is your **H**\ *igh-*\ **E**\ *nd* **I**\ *nteroperability* **M**\ *odule when* **D**\ *ata is* **ALL** *over the place*.
It's a bridge between scattered islands of data. ⚔️🌉🌈



In practice
===========

.. index::
   single: versions
   single: implementations
.. _version:

|project| implements the :external+hera:doc:`HERA schema <design/concepts>`.
Each of these implementations, named *connectors*, allows importing/exporting from/to a specific format.
All together, these connectors make interoperability between multiple systems easier (thanks to utility functions supported by a common vocabulary) and more durable (thanks to :external+datasphere:doc:`modular architecture <design/modularity>` and :external+datasphere:doc:`resilient project management <design/resilience>`).

There are two versions of |project| :

* :ref:`pyheimdall`, for the **Python** ecosystem: |doi-pyheimdall|
* :ref:`xheimdall`, for the **XQuery** ecosystem: |doi-xheimdall|

| These two versions are as compatible as possible, but their functional scope can differ.
  In short, :ref:`xheimdall` is the historical version, kinda confidential but seemingly sufficient for XQuery-related needs.
  On the other hand, :ref:`pyheimdall` is more modern and offers a much wider range of features, as well as more connectors.
| For more details, see the sections of this documentation dedicated to each version.



.. toctree::
   :hidden:

   /this
   /contributing
   /python/index
   /xquery/index
   /faq
   /genindex
   /search

.. include:: /doi.rst

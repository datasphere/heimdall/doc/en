:orphan:

=================
table of contents
=================

.. _toc:

.. toctree::

   /this
   /contributing
   /python/index
   /xquery/index
   /faq
   /genindex
